﻿using UnityEngine;

public static class StaticWaitForSeconds
{
   /* public static WaitForSeconds wfs0_1 = new WaitForSeconds(0.1f);
    public static WaitForSeconds wfs1 = new WaitForSeconds(1f);
    public static WaitForSeconds wfs5 = new WaitForSeconds(5f);
    public static WaitForSeconds wfs10 = new WaitForSeconds(10f);
    public static WaitForSeconds wfs15 = new WaitForSeconds(15f);
    public static WaitForSeconds wfs30 = new WaitForSeconds(30f);*/
    public static WaitForSeconds wfs0_01 = new WaitForSeconds(0.01f);
    public static WaitForSeconds wfs0_15 = new WaitForSeconds(0.15f);
    public static WaitForSeconds wfs0_5 = new WaitForSeconds(0.5f);
    public static WaitForSeconds wfs3 = new WaitForSeconds(3f);
    public static WaitForSeconds wfs2 = new WaitForSeconds(2f);
    public static WaitForSecondsRealtime wfs2_unscaled = new WaitForSecondsRealtime(2f);
    public static WaitForSeconds wfs1_5 = new WaitForSeconds(1.5f);
}