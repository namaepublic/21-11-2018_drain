﻿public static class StaticStrings
{
	public const string HitPlayer = "HitPlayer";
	public const string HitEnemy = "HitEnemy";
	public const string Index = "Index";
	public const string Win = "Win";
	public const string Close = "Close";
	public const string Open = "Open";
	public const string OpenBoss = "OpenBoss";
}
