﻿using UnityEngine;

public class AnimationBehaviour : MonoBehaviour
{
	[SerializeField] private Animator _animator;
	[SerializeField] private float _startAt = 0f;
	[SerializeField] private float _speed = 1f;
	[SerializeField] private bool _random = false;
	[SerializeField] [HideInInspector] private int _fullPathHash;

	private void OnEnable()
	{
		Play();
	}

	private void Play()
	{
		if (_random) _animator.Play(_fullPathHash, -1, Random.Range(0f, 1f));
		else 		 _animator.Play(_fullPathHash, -1, _startAt);
		_animator.speed = _speed;
	}


	private void OnValidate()
	{
		_animator = GetComponent<Animator>();
	}
}
