﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiController : MonoBehaviour
{
	[Header("Menu")]
	[SerializeField] private GameObject _menuPanel;
	[Header("Game")]
	[SerializeField] private GameObject _gamePanel;
	[Header("EndGame")]
	[SerializeField] private GameObject _endGamePanel;
	[Header("Other")]
	[SerializeField] private GameController _gameController;
	
	public void Init()
	{
		_menuPanel.SetActive(true);
		_gamePanel.SetActive(false);
		_endGamePanel.SetActive(false);
	}

	public void GamePanel()
	{
		_menuPanel.SetActive(false);
		_gamePanel.SetActive(true);
	}

	public void EndGamePanel(bool win)
	{
		
		_gamePanel.SetActive(false);
		_endGamePanel.SetActive(true);
		if (win)
		{
			
		}
		else
		{
			
		}
	}

	public void MenuPanel()
	{
		
		_menuPanel.SetActive(true);
		_gamePanel.SetActive(false);
		_endGamePanel.SetActive(false);
	}

}
