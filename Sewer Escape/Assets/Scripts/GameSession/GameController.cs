﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private BoatController _boatController;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private UiController _uiController;
    [SerializeField] private Transform _respawn;
    [SerializeField] private Image[] _livesImages;

    [SerializeField] private int _maxLives;
    [SerializeField] private int _lives;

    public bool CanLoseLife { get; set; }

    private bool _inGame;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _cameraController = FindObjectOfType<CameraController>();
        _respawn = GameObject.FindWithTag("Respawn").transform;
        if (!EditorApplication.isPlaying)
            _cameraController.Target = _respawn;
    }
#endif

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        foreach (var lifeImage in _livesImages)
            lifeImage.color = Color.white;

        _lives = _maxLives;
        CanLoseLife = true;
        _uiController.Init();
        _playerController.Init();
        _boatController.Init();
        _cameraController.Target = _respawn;
    }

    private void GameOver()
    {
        _playerController.Death();
        _boatController.Death();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Update()
    {
        if (!_inGame) return;
        _playerController.UpdatePlayer();
        _boatController.UpdateBoat();
        if (_playerController.IsStop && _boatController.IsStop)
        {
            _playerController.Play(_playerController.transform.position);
        }
    }

    private void FixedUpdate()
    {
        if (!_inGame) return;
        _playerController.FixedUpdatePlayer();
        _boatController.FixedUpdateBoat();
        _cameraController.FixedUpdateCamera();
    }

    public void Play()
    {
        _inGame = true;
        _playerController.Play(_respawn.position);
        _uiController.GamePanel();
    }

    public void EndGame(bool win)
    {
        _uiController.EndGamePanel(win);

        if (win)
        {
        }
        else
        {
        }
    }

    public void ReturnToMenu()
    {
        _uiController.MenuPanel();
    }

    public void AddLives(byte amount = 1)
    {
        _lives += amount;
    }

    public void RemoveLives(byte amount = 1)
    {
        if (!CanLoseLife) return;

        _lives -= amount;
        _livesImages[_lives].color = Color.black;
        CanLoseLife = false;
        if (_lives <= 0)
            GameOver();
    }
}