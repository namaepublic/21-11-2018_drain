﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    [SerializeField] private Vector3 _offset;

    public Transform Target { private get; set; }

#if UNITY_EDITOR
    private void Update()
    {
        if (EditorApplication.isPlaying) return;
        FixedUpdateCamera();
    }
#endif

    public void FixedUpdateCamera()
    {
        transform.position = Target != null ? Target.position + _offset : _offset;
    }
}