﻿using System;
using UnityEngine;

public class BoatController : MonoBehaviour
{
    [Flags]
    private enum BoatMovement : byte
    {
        NONE = 0,
        UP = 1,
        DOWN = 2
    }

    [SerializeField] private GameController _gameController;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Collider2D[] _colliders;
    [SerializeField] private Animator _animator;
    [SerializeField] private float _speed;
    [SerializeField] private float _dockingDistance;
    [SerializeField] private float _knockBackForce;

    [LayerField("Ground")] [SerializeField]
    private int _groundLayer;

    [LayerField("Trap")] [SerializeField] private int _trapLayer;

    [AnimatorParameter("LoseLife")] [SerializeField]
    private int _loseLifeId;

    private BoatMovement _movement;
    private bool _isDead, _stop, _hit;

    public bool IsStop
    {
        get { return _stop; }
    }

    private void OnValidate()
    {
        _gameController = FindObjectOfType<GameController>();
        _cameraController = FindObjectOfType<CameraController>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _colliders = GetComponents<Collider2D>();
        _animator = GetComponent<Animator>();
    }

    public void Init()
    {
        _isDead = false;
        _hit = false;
        gameObject.SetActive(true);
        Stop(true);
    }

    public void Play(Vector3 position, Vector2 velocity)
    {
        Stop(false);
        SetPosition(position);
        _rigidbody2D.velocity = velocity;
        _cameraController.Target = transform;
    }

    public void Stop(bool stop)
    {
        _stop = stop;
        _rigidbody2D.isKinematic = stop;
        _animator.enabled = !stop;
        _spriteRenderer.enabled = !stop;
        foreach (var collider in _colliders)
            collider.enabled = !stop;
    }

    public void UpdateBoat()
    {
        if (_isDead || _stop || _hit) return;

        _movement = BoatMovement.NONE;
        GetKey(BoatMovement.UP, KeyCode.Z, KeyCode.UpArrow);
        GetKey(BoatMovement.DOWN, KeyCode.S, KeyCode.DownArrow);
    }

    public void FixedUpdateBoat()
    {
        if (_isDead || _stop || _hit) return;

        if (CheckMovement(BoatMovement.UP))
        {
            _rigidbody2D.position += Vector2.up * _speed * Time.deltaTime;
        }

        if (CheckMovement(BoatMovement.DOWN))
        {
            _rigidbody2D.position += Vector2.down * _speed * Time.deltaTime;
        }

        _playerController.SetPosition(transform.position);
    }

    public void Death()
    {
        if (_isDead) return;
        _isDead = true;
    }

    public void Win()
    {
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    private void GetKey(BoatMovement movement, params KeyCode[] keys)
    {
        foreach (var key in keys)
            if (Input.GetKey(key))
                _movement |= movement;
    }

    private bool CheckMovement(BoatMovement movement)
    {
        return (_movement & movement) == movement;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        TrySwitch(other);
        CheckTrap(other);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        TrySwitch(other);
        CheckTrap(other);
    }

    private void TrySwitch(Component other)
    {
        if (!_isDead && !_stop && !_hit && other.gameObject.layer == _groundLayer)
            Switch();
    }

    private void CheckTrap(Component other)
    {
        if (!_isDead && !_stop && !_hit && other.gameObject.layer == _trapLayer)
            HitTrap();
    }

    private void Switch()
    {
        switch (_movement)
        {
            case BoatMovement.NONE:
                _playerController.Play(transform.position + (Vector3) (_rigidbody2D.velocity.normalized * _dockingDistance));
                break;
            case BoatMovement.UP:
                _playerController.Play(transform.position + Vector3.up * _dockingDistance);
                break;
            case BoatMovement.DOWN:
                _playerController.Play(transform.position + Vector3.down * _dockingDistance);
                break;
        }

        Stop(true);
    }

    private void HitTrap()
    {
        if (!_gameController.CanLoseLife) return;
        KnockBack();
        _hit = true;
        _gameController.RemoveLives();
        _animator.SetTrigger(_loseLifeId);
    }

    private void KnockBack()
    {
        _rigidbody2D.AddForce(-_rigidbody2D.velocity.normalized * _knockBackForce, ForceMode2D.Impulse);
    }

    public void CanLoseLife()
    {
        _hit = false;
        _gameController.CanLoseLife = true;
    }
}