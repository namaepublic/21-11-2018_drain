﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Flags]
    private enum PlayerMovement : byte
    {
        NONE = 0,
        LEFT = 1,
        RIGHT = 2,
        UP = 4,
        DOWN = 8
    }

    [SerializeField] private GameController _gameController;
    [SerializeField] private BoatController _boatController;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Collider2D[] _colliders;
    [SerializeField] private Animator _animator;
    [SerializeField] private float _speed;
    [SerializeField] private float _sailingDistance;
    [SerializeField] private float _knockBackForce;

    [LayerField("Water")] [SerializeField] private int _waterLayer;

    [LayerField("Trap")] [SerializeField] private int _trapLayer;

    [AnimatorParameter("LoseLife")] [SerializeField]
    private int _loseLifeId;

    private PlayerMovement _movement;
    private bool _isDead, _stop, _hit;

    public bool IsStop
    {
        get { return _stop; }
    }

    private void OnValidate()
    {
        _gameController = FindObjectOfType<GameController>();
        _cameraController = FindObjectOfType<CameraController>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _colliders = GetComponents<Collider2D>();
        _animator = GetComponent<Animator>();
    }

    public void Init()
    {
        _isDead = false;
        _hit = false;
        gameObject.SetActive(true);
        Stop(true);
    }

    public void Play(Vector3 position)
    {
        Stop(false);
        SetPosition(position);
        _cameraController.Target = transform;
    }

    public void Stop(bool stop)
    {
        _stop = stop;
        _rigidbody2D.isKinematic = stop;
        _animator.enabled = !stop;
        _spriteRenderer.enabled = !stop;
        foreach (var collider in _colliders)
            collider.enabled = !stop;
    }

    public void UpdatePlayer()
    {
        if (_isDead || _stop || _hit) return;

        _movement = PlayerMovement.NONE;
        GetKey(PlayerMovement.LEFT, KeyCode.Q, KeyCode.LeftArrow);
        GetKey(PlayerMovement.RIGHT, KeyCode.D, KeyCode.RightArrow);
        GetKey(PlayerMovement.UP, KeyCode.Z, KeyCode.UpArrow);
        GetKey(PlayerMovement.DOWN, KeyCode.S, KeyCode.DownArrow);
    }

    public void FixedUpdatePlayer()
    {
        if (_isDead || _stop || _hit) return;

        if (CheckMovement(PlayerMovement.LEFT))
        {
            _rigidbody2D.position += Vector2.left * _speed * Time.deltaTime;
        }

        if (CheckMovement(PlayerMovement.RIGHT))
        {
            _rigidbody2D.position += Vector2.right * _speed * Time.deltaTime;
        }

        if (CheckMovement(PlayerMovement.UP))
        {
            _rigidbody2D.position += Vector2.up * _speed * Time.deltaTime;
        }

        if (CheckMovement(PlayerMovement.DOWN))
        {
            _rigidbody2D.position += Vector2.down * _speed * Time.deltaTime;
        }

        _boatController.SetPosition(transform.position);
    }

    public void Death()
    {
        if (_isDead) return;
        _isDead = true;
    }

    public void Win()
    {
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    private void GetKey(PlayerMovement movement, params KeyCode[] keys)
    {
        foreach (var key in keys)
            if (Input.GetKey(key))
                _movement |= movement;
    }

    private bool CheckMovement(PlayerMovement movement)
    {
        return (_movement & movement) == movement;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        TrySwitch(other);
        CheckTrap(other);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        TrySwitch(other);
        CheckTrap(other);
    }

    private void TrySwitch(Component other)
    {
        if (!_isDead && !_stop && !_hit && other.gameObject.layer == _waterLayer)
            Switch();
    }

    private void CheckTrap(Component other)
    {
        if (!_isDead && !_stop && !_hit  && other.gameObject.layer == _trapLayer)
            HitTrap();
    }

    private void Switch()
    {
        switch (_movement)
        {
            case PlayerMovement.NONE:
                _boatController.Play(transform.position + (Vector3) (_rigidbody2D.velocity.normalized * _sailingDistance),
                    _rigidbody2D.velocity);
                break;
            case PlayerMovement.LEFT:
                _boatController.Play(transform.position + Vector3.left * _sailingDistance, _rigidbody2D.velocity);
                break;
            case PlayerMovement.RIGHT:
                _boatController.Play(transform.position + Vector3.right * _sailingDistance, _rigidbody2D.velocity);
                break;
            case PlayerMovement.UP:
                _boatController.Play(transform.position + Vector3.up * _sailingDistance, _rigidbody2D.velocity);
                break;
            case PlayerMovement.DOWN:
                _boatController.Play(transform.position + Vector3.down * _sailingDistance, _rigidbody2D.velocity);
                break;
        }

        Stop(true);
    }

    private void HitTrap()
    {
        if (!_gameController.CanLoseLife) return;
        KnockBack();
        _hit = true;
        _gameController.RemoveLives();
        _animator.SetTrigger(_loseLifeId);
    }

    private void KnockBack()
    {
        if (_rigidbody2D.velocity != Vector2.zero)
        {
            _rigidbody2D.AddForce(-_rigidbody2D.velocity.normalized * _knockBackForce, ForceMode2D.Impulse);
            return;
        }
        switch (_movement)
        {
            case PlayerMovement.LEFT:
                _rigidbody2D.AddForce(Vector2.right * _knockBackForce, ForceMode2D.Impulse);
                break;
            case PlayerMovement.RIGHT:
                _rigidbody2D.AddForce(Vector2.left * _knockBackForce, ForceMode2D.Impulse);
                break;
            case PlayerMovement.UP:
                _rigidbody2D.AddForce(Vector2.down * _knockBackForce, ForceMode2D.Impulse);
                break;
            case PlayerMovement.DOWN:
                _rigidbody2D.AddForce(Vector2.up * _knockBackForce, ForceMode2D.Impulse);
                break;
        }
    }

    public void CanLoseLife()
    {
        _hit = false;
        _gameController.CanLoseLife = true;
    }
}