﻿using UnityEngine;

public class LayerFieldAttribute : PropertyAttribute
{
    public int LayerId { get; private set;  }
    
    public bool HideInInspector { get; private set;  }
    
    public LayerFieldAttribute(string layerName, bool hideInInspector = false)
    {
        LayerId = LayerMask.NameToLayer(layerName);
        HideInInspector = hideInInspector;
    }
}