﻿using UnityEngine;

public class AnimatorParameterAttribute : PropertyAttribute 
{
	public int ParameterId { get; private set;  }
    
	public bool HideInInspector { get; private set;  }
    
	public AnimatorParameterAttribute(string parameterName, bool hideInInspector = false)
	{
		ParameterId = Animator.StringToHash(parameterName);
		HideInInspector = hideInInspector;
	}
}
